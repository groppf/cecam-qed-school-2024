# Pair Review Plan

## List of Reviewers

- Ruggi
- Markus
- Simone
- Dominik
- Norah
- I-Te
- Enrico
- Heiko
- Mark
- Frieder
- Ilke
- Carlos
- Franco

## Notebooks

- Notebook 1 - [Quantum Harmonic Oscillator](notebook1.ipynb)   ->  Notebook1_QHO
- Notebook 2 - [1D particle](notebook2.ipynb)   -> Notebook2_1p
- Notebook 3 - [1D particle - 1 mode](notebook3.ipynb)  ->  Notebook3_1p1m
- Notebook 4 - [1D particle - Many modes](notebook4.ipynb)  ->  Notebook4_1pMm
- Notebook 5 - [Mathematical Inconsistencies](notebook5.ipynb)  -> Notebook6_math
- Notebook 6 - [Many particles - Many modes](notebook6.ipynb)   -> Notebook5_MpMm
- Notebook 7 - [Polaritons](notebook7.ipynb)    ->  Notebook7_pol
- Notebook 7++ - [2D Exciton Polaritons](notebook7_mw.ipynb)    ->  Notebook8_poldeluxe
- Notebook 8 - [Photo-Groundstates](notebook8.ipynb)    -> Notebook9_PG
- Notebook 10 - [Cavity MD for CO2](notebook10.ipynb)   ->  Notebook10_CMD
- Notebook 11 - [1D QEDFT](notebookXXX_I-Te-Lu.ipynb)   ->  Notebook11_QEDFT
- Notebook 12 - [Macroscopic Quantum Electrodynamics: Describing real cavities](notebook13.ipynb)   ->  Notebook12_MQED

## Tutorials

1. Tutorial 1 - [Cavity Modified Band Structure](tutorial-3.2.2_I-Te-Lu.ipynb)  ->  Tutorial1_CBS
2. Tutorial 2 - [Optical Spectra Cavity-Benzene](linear_response/casida/Optical_spectra_from_casida.ipynb)  ->  Tutorial2_spec
3. Tutorial 3 - [External Fields](tutorial_tddft_mxll/tutorial_ext_source.ipynb)    ->  Tutorial3_extF
4. Tutorial 4 - [Full Minimal Coupling](tutorial_tddft_mxll/tutorial_full_minimal.ipynb)    ->  Tutorial4_FMC

## Review Schedule

| Notebook     | Responsible   | Reviewer          | Date       |
|--------------|---------------|-------------------|------------|
| Notebook 1   | Ruggi   (X)   | Ilke (X), Carlos (X)  | 05-04-2024 |
| Notebook 2   | Ruggi   (X)   | Ilke (X), Carlos (X)  | 05-04-2024 |
| Notebook 3   | Ruggi   (X)   | Markus, I-Te      | 05-04-2024 |
| Notebook 4   | Ruggi   (X)   | Enrico, Heiko     | 05-04-2024 |
| Notebook 5   | Markus        | Norah(X), Mark    | 05-04-2024 |
| Notebook 6   | Ruggi         | Enrico, Heiko     | 05-04-2024 |
| Notebook 7   | Simone  (X)   | Franco (X), I-Te      | 05-04-2024 |
| Notebook 7++ | Simone  (X)   | Mark, I-Te        | 05-04-2024 |
| Notebook 8   | Simone  (X)   | Dominik, Markus   | 05-04-2024 |
| Notebook 10  | Dominik       | Ruggi, Simone     | 05-04-2024 |
| Notebook 11  | I-Te          | Simone (X), Ilke  | 05-04-2024 |
| Notebook 13  | Mark          | Franco (X), Markus    | 05-04-2024 |
| Tutorial 1   | I-Te          | Norah(X), Heiko   | 05-04-2024 |
| Tutorial 2   | Norah    (X)  | Carlos (X), Simone    | 05-04-2024 |
| Tutorial 3-4 | Franco        | Mark, Frieder (x) | 05-04-2024 |
